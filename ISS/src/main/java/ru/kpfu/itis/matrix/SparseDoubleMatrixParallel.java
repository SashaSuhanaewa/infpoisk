package ru.kpfu.itis.matrix;

import java.util.concurrent.atomic.AtomicInteger;

public class SparseDoubleMatrixParallel {
    private double[] values;
    private int[] columns;
    private int[] rowPointers;

    public SparseDoubleMatrixParallel(int[] rowPointers, int[] columns, double[] values) {
        if (rowPointers.length != values.length) {
            throw new IllegalArgumentException();
        }

        this.values = values;
        this.columns = columns;
        this.rowPointers = rowPointers;
    }

    public double[] multiplyByVector(double[] v) {
        int size = values.length;
        double[] res = new double[size];

        AtomicInteger colIndex = new AtomicInteger(0);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < rowPointers[i]; j++) {
                int k = colIndex.getAndIncrement();
                res[columns[k]] += v[columns[k]] * values[i];
            }
        }

        return res;
    }

    public int size() {
        return values.length;
    }
}
