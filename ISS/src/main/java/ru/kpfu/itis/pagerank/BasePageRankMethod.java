package ru.kpfu.itis.pagerank;

import ru.kpfu.itis.matrix.SparseDoubleMatrix;

public abstract class BasePageRankMethod implements IPageRankMethod {
    protected abstract double[] getFirstApproximation(double[][] matrix);
    protected abstract double[] getFirstApproximation(SparseDoubleMatrix matrix);

    protected abstract double[] produceStep(double[] vector, double[][] matrix);
    protected abstract double[] produceStep(double[] vector, SparseDoubleMatrix matrix);

    protected double[] normalizeVector(double[] vector) {
        double len = 0;
        for (double d : vector) {
            len += d*d;
        }
        len = Math.sqrt(len);

        double[] newVector = new double[vector.length];
        for (int i = 0; i < vector.length; i++) {
            newVector[i] = vector[i] / len;
        }
        return newVector;
    }

    protected boolean checkApproximation(double[] vector, double[] prevVector, double eps) {
        double sum = 0;
        for (int i = 0; i < prevVector.length; i++) {      //by Manhattan distance
            sum += Math.abs(vector[i] - prevVector[i]);
        }
        return sum < eps;
    }

    /*protected boolean checkApproximation(double[] vector, double[] prevVector, double eps) {
        for (int i = 0; i < prevVector.length; i++) {      //by single comparison distance
            if ( Math.abs(vector[i] - prevVector[i]) >= eps) {
                return false;
            }
        }
        return true;
    }*/

    @Override
    public double[] calculatePageRank(double[][] matrix, double eps) {
        double[] vector = getFirstApproximation(matrix);
        double[] prevVector;

        do {
            prevVector = vector;
            vector = normalizeVector( produceStep(vector, matrix) );
        } while ( ! checkApproximation(vector, prevVector, eps) );

        return vector;
    }

    @Override
    public double[] calculatePageRank(SparseDoubleMatrix matrix, double eps) {
        double[] vector = getFirstApproximation(matrix);
        double[] prevVector;

        do {
            prevVector = vector;
            vector = normalizeVector( produceStep(vector, matrix) );
        } while ( ! checkApproximation(vector, prevVector, eps) );

        return vector;
    }
}
